# Empty Project for PolySync 2.x with C++ #

This is a basic template for creating applications using the PolySync 2.x C++ API.

To customize this template for your application, simply replace (and rename) all instances of MyNode
with the name of your application in the following files:

    CMakeLists.txt
    include/MyNode.h
    src/CMakeLists.txt
    src/main.cpp
    src/MyNode.cpp
