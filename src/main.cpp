#include "MyNode.hpp"
#include <iostream>
#include <unistd.h>

int main(int argc, char* argv[])
{
    int c;
    bool logging = false;

    while ((c = getopt(argc, argv, "ho")) != -1)
    {
        switch (c)
        {
            case 'h':
                printf("%s \n", "");
                printf("%s \n", "Empty PolySync 2.x C++ Application");
                printf("%s \n", "    -h        Show this help menu and exit.");
                printf("%s \n", "    -o        Enable PolySync logging output.");
                printf("%s \n", "");
                return 1;
            case 'o':
                logging = true;
                break;
            default:
                abort();
        }
    }

    MyNode mn(logging);

    mn.connectPolySync();

    return 0;
}
