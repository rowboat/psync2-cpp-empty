#include "MyNode.hpp"

using namespace std;
using namespace polysync;

//Default constructor.
MyNode::MyNode(bool enableLogging) :
    Node(),
    enableLogging(enableLogging)
{
}

//Default destructor.
MyNode::~MyNode()
{
}

//Can handle arguments here, if using the constructor with command line arguments.
/*void MyNode::setConfigurationEvent(int argc, char * argv[])
{
}*/

//Register listeners for message types here.
void MyNode::initStateEvent()
{
    setNodeType(PSYNC_NODE_TYPE_API_USER);
    setDomainID(PSYNC_DEFAULT_DOMAIN);
    setSDFID(PSYNC_SDF_ID_INVALID);
    if (enableLogging)
        setFlags(0 | PSYNC_INIT_FLAG_STDOUT_LOGGING);
    else
        setFlags(0);
    setNodeName("MyNode");

    //How to register message listeners.
    ps_msg_type type = getMessageTypeByName("ps_byte_array_msg");
    registerListener(type);
}

//Clean up resources in this function.
void MyNode::releaseStateEvent()
{
}

void MyNode::errorStateEvent()
{
}

void MyNode::fatalStateEvent()
{
}

void MyNode::warnStateEvent()
{
}

//Add time-dependent message publishing here.
//Node enters this state on every loop if not in another state.
void MyNode::okStateEvent()
{
}

//Handle incoming messages here. This is handled in a
//separate node from the main thread and is, thereofre, non-blocking.
void MyNode::messageEvent(shared_ptr<Message> message)
{
    //Example of message filtering.
    if (auto diagMsg = datamodel::getSubclass<datamodel::DiagnosticTraceMessage>(message))
    {
        diagMsg->print();
    }
}
