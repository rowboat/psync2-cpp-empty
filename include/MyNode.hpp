#ifndef MYNODE_HPP
#define MYNODE_HPP

//C++ Includes
#include <iostream>

//OS Includes
#include <unistd.h>

//PolySync Includes
#include <PolySyncNode.hpp>
#include <PolySyncMessage.hpp>
#include <PolySyncDataModel.hpp>

class MyNode : public polysync::Node
{
    public:
        MyNode(bool enableLogging);
        ~MyNode();
        void run();
    private:
        void initStateEvent() override;
        void releaseStateEvent() override;
        void errorStateEvent() override;
        void fatalStateEvent() override;
        void warnStateEvent() override;
        void okStateEvent() override;
        void messageEvent(std::shared_ptr<polysync::Message> message) override;
        bool enableLogging = false;
};

#endif
